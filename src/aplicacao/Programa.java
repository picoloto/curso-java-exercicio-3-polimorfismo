package aplicacao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entidades.Produto;
import entidades.ProdutoImportado;
import entidades.ProdutoUsado;

public class Programa {

	public static void main(String[] args) throws ParseException {

		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		List<Produto> produtos = new ArrayList<>();

		System.out.print("N�mero de Produtos: ");
		int n = sc.nextInt();

		for (int i = 1; i <= n; i++) {
			System.out.println();

			System.out.println("Dados do Produto #" + i);
			System.out.print("Novo, usado ou importado (n/u/i)? ");
			char t = sc.next().charAt(0);
			sc.nextLine();

			System.out.print("Nome: ");
			String nome = sc.nextLine();

			System.out.print("Pre�o: ");
			double preco = sc.nextDouble();

			if (t == 'n') {
				produtos.add(new Produto(nome, preco));
			} else if (t == 'u') {
				System.out.print("Data de fabrica��o (DD/MM/YYYY): ");
				Date dataFabricacao = sdf.parse(sc.next());

				produtos.add(new ProdutoUsado(nome, preco, dataFabricacao));
			} else {
				System.out.print("Taxa: ");
				double taxa = sc.nextDouble();

				produtos.add(new ProdutoImportado(nome, preco, taxa));
			}

		}

		System.out.println();
		
		System.out.println("Tags de Pre�o");
		for (Produto p : produtos) {
			System.out.println(p.tagPreco());
		}

		sc.close();

	}

}
